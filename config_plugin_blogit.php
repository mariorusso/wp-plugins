<?php
/*
* Plugin Name: Blogit Configuration
* Description: Sitewide configuration options for Blogit site
* Version: 1.0 
* Author: Mario Russo
* Author URI: http://www.russomario.com
*  
*
* 
*/
// Register Custom Post Type
function add_prepared_food() {

	$labels = array(
		'name'                => _x( 'Prepared Foods', 'Prepared Foods'),
		'singular_name'       => _x( 'Prepared Food', 'Prepared Food' ),
		'menu_name'           => __( 'Prepared Foods' ),
		'parent_item_colon'   => __( 'Parent Item:' ),
		'all_items'           => __( 'All Items' ),
		'view_item'           => __( 'View Item'),
		'add_new_item'        => __( 'Add New Item' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Item' ),
		'update_item'         => __( 'Update Item' ),
		'search_items'        => __( 'Search Item' ),
		'not_found'           => __( 'Not found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$args = array(
		'label'               => __('Prepared Foods'),
		'description'         => __('Especific type of post to prepared foods'),
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes' ),
		'taxonomies'          => array('category'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'prepared_food', $args );

}

// Hook into the 'init' action
add_action( 'init', 'add_prepared_food', 0 );


//Add featured image to theme
if(function_exists('add_theme_support')) {
    
  add_theme_support( 'post-thumbnails' );
  
}


//Add excerpt for pages.
if(function_exists('add_post_type_support')) {

	add_post_type_support( 'page', 'excerpt');

}

