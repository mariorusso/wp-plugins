<?php
/*
* Plugin Name: My Youtube Video
* Description: Add remote video with short code
* Version: 1.0 
* Author: Mario Russo
* Author URI: http://www.russomario.com
*/

function my_youtube_video($attr) {
	
	$src =(isset($attr['src'])) ? $attr['src'] : '';
	
   if(!isset($attr['src'])) {
		$video = '<span>Sorry no Video provided!</span>';
	}else{
		$video = "<iframe width='100%' height='500' src='$src' frameborder='0' allowfullscreen></iframe>";
	}
	 
	 return $video;
	
}

add_shortcode('video', 'my_youtube_video');
