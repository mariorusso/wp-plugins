<?php
/*
* Plugin Name: My System Alert Plugin
* Description: Alert box from admin
* Version: 1.0 
* Author: Mario Russo
* Author URI: http://www.russomario.com
*/

function my_system_alert($content){
	$alert = "<div class='alert'>My Alert!!!</div>";
	return $alert . $content;
}
add_filter('the_content', 'my_system_alert');

function alert_css(){
		$css = "<style>
					div.alert{
						width: 80%;
						margin: 10px auto;
						padding: 10px;
						border: 3px solid #f00;
						background: #FFFF99;
						font-size: 130%;
						color: #f00;
						text-align: center;
					}
				</style>";
			echo $css;
}
add_action('wp_head', 'alert_css');

/*---------------ADMINISTRATION--------------------------------------------------------------
 ___________________________________________________________________________________________*/
 
 add_action('admin_menu', 'my_system_alert_admin' );
 add_action('admin_menu', 'register_sysalert_options' );
 add_action('admin_menu', 'add_sysalert_sections');
 add_action('admin_menu', 'add_sysalert_fields');
 
 //Add menu link to dashboard
 function my_system_alert_admin(){
 	add_options_page('System Alert', 'System Alert Setings', 'manage_options', 'sysalert', 'sysalert_admin_options');
 }


//Add basic content to menu page
function sysalert_admin_options(){
	
	echo '<div class="wrap">';
	echo '<h1> System Alert Settings </h1>';
	
	echo'<form method="post" action="options.php">';
		settings_fields('sysalert');
		do_settings_sections('sysalert');
		
	submit_button();
	
	echo '</form>';
	
	echo '</div><!--/wrap-->';	
}

function register_sysalert_options(){
	register_setting( 'sysalert',// Option Group
					  'sysalert_options', // name of the field db
					  'sanatize_sysalert_options' // callback function
					  ); 
}

function add_sysalert_sections(){
	add_settings_section(
	'general_options', //id of the section
	'General Options', // Section Title 
	'', //callback function
	'sysalert' //slug of menu page
		
	);
	
}

function add_sysalert_fields() {
	
	add_settings_field(
		'sysalert_message', //id for field
		'Alert Message', // title / label of field
		'create_message_field', //callback is the one that echo the field
		'sysalert', //slug of menu page field shoud appear on
		'general_options' // 
	);
	
	add_settings_field(
		'sysalert_message_type', //id for field
		'Alert Message Type', // title / label of field
		'create_message_field', //callback is the one that echo the field
		'sysalert', //slug of menu page field shoud appear on
		'general_options' // 
	);
}

function create_message_field() {
	echo "<input type='text' id='sysalert_message' name='' value='' />";
}
