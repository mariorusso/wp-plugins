<?php
/*
* Plugin Name: Remote Image
* Description: Add remote image with short code
* Version: 1.0 
* Author: Mario Russo
* Author URI: http://www.russomario.com
*/

function my_remote_image($attr) {
	
	$src =(isset($attr['src'])) ? $attr['src'] : '';
	
	$alt =(isset($attr['alt'])) ? $attr['alt'] : 'Image';
	
	if(!isset($attr['src'])) {
		$image = '<span>Sorry no image provided!</span>';
	}else{
		$image = "<img src='$src' alt='$alt' />";
	}
	 
	 return $image;
	
}

add_shortcode('image', 'my_remote_image');
