<?php
/*
* Plugin Name: Hello ShortCode
* Description: Say Hello Shortcode
* Version: 1.0 
* Author: Mario Russo
* Author URI: http://www.russomario.com
*/

function my_say_hello($attr) {
	
	$text = (isset($attr['text'])) ? $attr['text'] : 'Hello, World!';
	
	return '<h1>' . $text . '</h1>';
	
}

add_shortcode('hello', 'my_say_hello');
