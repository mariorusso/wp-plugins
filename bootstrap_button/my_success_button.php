<?php
/*
* Plugin Name: Bootstrap Button Shortcode
* Description: Shortcode to insert a button with short code
* Version: 1.0 
* Author: Mario Russo
* Author URI: http://www.russomario.com
*/

function my_button_maker($attr) {
	
	$link = (isset($attr['link'])) ? $attr['link'] : '#';
	
	$type = (isset($attr['type'])) ? $attr['type'] : 'default';
	
	$text = (isset($attr['text'])) ? $attr['text'] : 'GO';
	
	$button = '<a class="btn btn-' . $type  . '" href=http://' . $link . '>' . $text  . '</a>';
	 
	 return $button;
	
}

add_shortcode('button', 'my_button_maker');

function button_bootstrap_css() {
	$css = plugin_dir_url(__FILE__) . 'bootstrap-button.css';
	
	wp_enqueue_style( 'button-bootstrap-css', $css );
	
	wp_enqueue_style('my-button-css', plugin_dir_url(__FILE__) . 'my-button.css', ['button-bootstrap-css']);
	

}
add_action( 'wp_enqueue_scripts', 'button_bootstrap_css' );