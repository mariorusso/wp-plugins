<?php
/*
* Plugin Name: My Callout Plugin
* Description: Using short code around the content
* Version: 1.0 
* Author: Mario Russo
* Author URI: http://www.russomario.com
*/

function my_callout_content($attr, $content = null) {
		 
	 return "<div class='callout'>$content</div>";
	
}

add_shortcode('make_callout', 'my_callout_content');

function print_css(){
	
	$css = '<style>
			.callout{
			 padding: 10px;
			 width: 80%;
			 margin: 10px auto;
			 border: 2px solid #f00;	
			 background: #eee;
			 font-size: 200%;
			 text-align: center;
			}
			</style>';
			
	echo $css;
	
}
add_action('wp_head', 'print_css');
